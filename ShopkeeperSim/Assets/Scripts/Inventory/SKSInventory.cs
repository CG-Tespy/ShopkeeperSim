﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEditor;

public class SKSItemEvent : UnityEvent<SKSItem> {}

[CreateAssetMenu(menuName = "Inventory/New Inventory", fileName = "NewSKSInventory")]
public class SKSInventory : GameInventory<SKSItem>
{
	protected virtual void OnEnable()
	{
		SetupEvents();
	}

	void SetupEvents()
	{
		ContentChanged = 							new UnityEvent();
		ItemAdded = 								new SKSItemEvent();
		ItemRemoved = 								new SKSItemEvent();
	}

	
}
